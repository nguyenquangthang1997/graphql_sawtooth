from aiohttp import web
from aiohttp_graphql import GraphQLView
import sys

sys.path.append("..")
from schema import schema

app = web.Application()
GraphQLView.attach(
    app,
    route_path='/graphql',
    schema=schema,
    graphiql=True)

if __name__ == '__main__':
    web.run_app(app=app, host="localhost", port=5000)
