import datetime
import os
from elasticsearch_dsl import connections
from elasticsearch_dsl import Search
from elasticsearch_dsl import (
    analyzer,
    Boolean,
    Completion,
    Date,
    Document,
    InnerDoc,
    Keyword,
    Nested,
    Text,
    Integer,
    Float
)
import sys

sys.path.append("..")
from rest_api.settings import ELASTICSEARCH_CONNECTION

try:
    from elasticsearch import logger
except ImportError:
    import logging

    logger = logging.getLogger(__name__)

es = connections.create_connection(**ELASTICSEARCH_CONNECTION)
class User(Document):

    encrypted_private_key = Text()
    hashed_password = Text()
    public_key = Text()
    role = Text()
    transactionIdBlockchain = Text()
    username = Text(fields={'raw': Keyword()})

    class Index:
        name = "trustion_drug_user"
        settings = {
            'number_of_shards': 1,
            'number_of_replicas': 1,
            'blocks': {'read_only_allow_delete': None},
        }

    def search_client(self, **kwargs):
        field = kwargs['field']
        search1 = super(User, self).search().using(client=es).query("match", username=field)
        return search1

    def create_client(self, **kwargs):
        self.hashed_password = kwargs['hashed_password']
        self.encrypted_private_key = kwargs['encrypted_private_key']
        self.public_key = kwargs['public_key']
        self.transactionIdBlockchain = kwargs['transactionIdBlockchain']
        self.role = kwargs['role']
        self.username = kwargs['username']
        # kwargs["body"] = body
        # kwargs['index'] = "trustion_drug_user"
        return super().save()


try:
    # Create the mappings in Elasticsearch
    User.init()
except Exception as err:
    logger.error(err)
