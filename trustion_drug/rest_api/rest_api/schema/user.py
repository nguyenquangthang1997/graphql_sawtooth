import graphene
from graphene import Node
from graphene_elastic import (
    ElasticsearchObjectType,
    ElasticsearchConnectionField,
)
from Crypto.Cipher import AES
import bcrypt

import sys
import uuid
import time
import datetime

sys.path.append("..")
from rest_api.document.user import User as UseDocument
from rest_api.messaging import Messenger
import logging
LOGGER = logging.getLogger(__name__)



__all__ = (
    "CreatePerson",
    'User',
    'Queries',
    'Mutations'
)


class User(ElasticsearchObjectType):
    class Meta:
        document = UseDocument
        interfaces = (Node,)


class Queries(graphene.ObjectType):
    search_by_name = graphene.List(User, field=graphene.String())

    async def resolve_search_by_name(self, *args, **kwargs):
        field = kwargs['field']
        a = UseDocument.search_client(UseDocument, field=field)
        return a

    search = graphene.List(User)

    async def resolve_search(self, *args, **kwargs):
        return UseDocument.search()


class PersonInput(graphene.InputObjectType):
    password = graphene.String(required=True)
    role = graphene.String(required=True)
    username = graphene.String(required=True)


def get_time():
    dts = datetime.datetime.utcnow()
    return round(time.mktime(dts.timetuple()) + dts.microsecond / 1e6)


class CreatePerson(graphene.Mutation):
    class Arguments:
        person_data = PersonInput(required=True)

    result = graphene.String()

    async def mutate(root, info, person_data=None):
        messenger = Messenger("tcp://validator:4004")
        messenger.open_validator_connection()
        public_key, private_key = messenger.get_new_key_pair()
        user_id = str(uuid.uuid4())
        LOGGER.info(person_data)
        transactionUnique = await messenger.send_create_user_transaction(
            private_key=private_key,
            role=person_data.role,
            username=person_data.username,
            timestamp=get_time(),
        )

        transactionUniqueId = transactionUnique.transactions[0].header_signature
        encrypted_private_key = encrypt_private_key(
            "ffffffffffffffffffffffffffffffff", public_key, private_key)
        hashed_password = hash_password(person_data.password)

        # elasticsearch.createUser(
        #     username=body.get('username'),
        #     role=role,
        #     hashed_password=hashed_password,
        #     encrypted_private_key=encrypted_private_key,
        #     public_key=public_key,
        #     transactionIdBlockchain=transactionUniqueId
        # )
        #

        UserDocument = UseDocument()

        person = UserDocument.create_client(
            hashed_password=hashed_password.hex(),
            role=person_data.role,
            username=person_data.username,
            encrypted_private_key=encrypted_private_key.hex(),
            transactionIdBlockchain=transactionUniqueId,
            public_key=public_key
        )

        return CreatePerson(result=person)


class Mutations(graphene.ObjectType):
    create_person = CreatePerson.Field()


def encrypt_private_key(aes_key, public_key, private_key):
    init_vector = bytes.fromhex(public_key[:32])
    cipher = AES.new(bytes.fromhex(aes_key), AES.MODE_CBC, init_vector)
    return cipher.encrypt(private_key)


def decrypt_private_key(aes_key, public_key, encrypted_private_key):
    init_vector = bytes.fromhex(public_key[:32])
    cipher = AES.new(bytes.fromhex(aes_key), AES.MODE_CBC, init_vector)
    private_key = cipher.decrypt(bytes.fromhex(encrypted_private_key))
    return private_key


def hash_password(password):
    return bcrypt.hashpw(bytes(password, 'utf-8'), bcrypt.gensalt())

# class Middleware(graphene.Schema):
#     def __init__(self, messenger, query, mutation):
#         messenger = messenger
#         super().__init__(query=query, mutation=mutation)
# schema = graphene.Schema(
#     query=Query,
#     mutation=Mutations
# )
